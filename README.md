## tstk - A toolkit for analysing sequencing data.

Install with conda:

```bash
conda install -c bioconda -c tdido tstk
```

or with pip:

```bash
pip install tstk
```
